vault
=====
Deploys Hashicorp Vault

Supported Operating Systems
---------------------------
* CentOS 7

Requirements
------------
No Requirements

Role Variables
--------------
None

Role Defaults
-------------
```
---
vault_root: <string> (where vault files are stored) /opt/hashicorp/vault/
vault_config: <string> (where vault configuration is stored) /etc/{{ vault_root }}
vault_version: <string> (vault version to deploy) 0.8.3
vault_checksum: <string> (checksum of the vault zip to deploy) sha256:a3b687904cd1151e7c7b1a3d016c93177b33f4f9ce5254e1d4f060fca2ac2626
vault_ip: <string> (ip for vault server to bind to) 0.0.0.0
vault_port: <int> (ip for vault server to listen on) 8200
vault_server: <boolean> (if vault should run as server) false
```

Dependencies
------------
None


Testing
-------
Tests can be found in the `tests` directory. Current tests validate the following:

* yamllint
* Syntax check
* Provisioning
* vault access after provisioning

### Local Testing
Local testing can be done by running the `docker-compose.yml` file found in the `tests` directory

### CI Testing
CI testing configuration can be found in `.gitlab-ci.yml` in the root directory

License
-------
ISC

Author Information
------------------
CV Admins <cvadmins@utdallas.edu>
