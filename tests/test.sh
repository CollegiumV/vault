#!/bin/sh -e
# Lint
echo "Linting role..."
yamllint ../ || true
# Syntax check
echo "Checking role syntax..."
ansible-playbook --syntax-check all.yml
# Ansible Playbook
echo "Running role..."
ansible-playbook all.yml
echo "Verify vault is running..."                                                                                                   
export VAULT_ADDR='http://centos7:8200'
/opt/hashicorp/vault/bin/vault init -key-shares=1 -key-threshold=1 > /root/vault_creds || true
UNSEAL=$(grep -i -m 1 unseal /root/vault_creds | cut -d' ' -f4)
ROOT=$(grep -i -m 1 root /root/vault_creds | cut -d' ' -f4)
/opt/hashicorp/vault/bin/vault unseal $UNSEAL
/opt/hashicorp/vault/bin/vault auth $ROOT
